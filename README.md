# Code Snippet to Upload files to S3 with `upload_file()`

## Step 1 - Import a file to let IBM Cloud generate the S3 Client

In the below example, the S3 Client object is `client_01fa29576c1949bf8bf19f9e93a1ba4a`, the bucket is `Bucket='christmasdinner-donotdelete-pr-wxb3ruu1hazbsw'`

![image](images/step1_ibm_boto.png)


## Step 2 - Write a file to the local temporary file system with `to_csv()`

This is the file, targeted to be written to S3 Storage.
```
some_data_frame.to_csv("output.csv")
```

## Step 3 - use the `client_01fa29576c1949bf8bf19f9e93a1ba4a` object to write the file to `S3`

For more information about `upload_file()` see

https://ibm.github.io/ibm-cos-sdk-python/reference/services/s3.html?highlight=upload_file#S3.Client.upload_file

General Documentation on

https://ibm.github.io/ibm-cos-sdk-python/reference/services/s3.html?highlight=upload_file#client

---

If you write it to the Projects' bucket - it will appear in the Projects Files.
The project Bucket can be copied from step 1.

Use similar code snippet below - to write file `output.csv` to bucket `Bucket`.
- `Bucket='christmasdinner-donotdelete-pr-wxb3ruu1hazbsw'`
- File to be written in this example is `output.csv` from step 2
- `Key` argument is the filename used in `S3`, which usually is the same as the filename


Execute the following code, with `upload_file()`, to upload the file.
```
Bucket='christmasdinner-donotdelete-pr-wxb3ruu1hazbsw'
client_01fa29576c1949bf8bf19f9e93a1ba4a.upload_file("output.csv", Bucket=Bucket, Key="output.csv")
```

When succesful, the file will appear on the project page, and can be added to Data Assets.

![image](images/project.png)